﻿// ===========================================
// kobukiをjoystickで操作するためのサンプルプログラム
// ===========================================

/* ヘッダファイルの読み込み */
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>

// 発展課題
// #include <kobuki_msgs/BumperEvent.h>
// #include <sensor_msgs/Imu.h>

/* クラス定義 */
class KobukiOperation
{
public:
    // 関数はこっち
    KobukiOperation(); // コンストラクタ(実行すると最初に呼ばれ、変数を初期化する役割)
    void joyCallback(const sensor_msgs::Joy::ConstPtr &joy); // joystickの信号を受け取る

    // 発展課題
    // void kobukiCallback(const sensor_msgs::Imu); // kobukiの信号を受け取る
    // void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper);

private:
    //変数はこっち
    ros::NodeHandle nh; // これは絶対いいる。　ノードを管理するための変数
    ros::Subscriber kobuki_sub, joy_sub, bumper_sub; // subscriberの変数宣言
    ros::Publisher  kobuki_pub; // publisherの変数宣言
    double          scaleLinear, scaleAngular; // 使いたい変数宣言(C言語と一緒の形式)
    int attack;
};

/* クラス内の関数 */
KobukiOperation::KobukiOperation()
{

    // subscriber
    joy_sub = nh.subscribe <sensor_msgs::Joy>("joy", 1, &KobukiOperation::joyCallback, this);
    // 発展課題
    // bumper_sub       = nh.subscribe <kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &KobukiOperation::bumperCallback, this);

    // publisher
    kobuki_pub = nh.advertise <geometry_msgs::Twist>("/mobile_base/commands/velocity", 1); //Kobuki実機

    // 変数の初期化
    scaleLinear  = 0.1;
    scaleAngular = 0.5;
    // attack=0;
}

//発展課題
// void KobukiOperation::bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
// {
//
//     attack = bumper->state;
// }


/* ジョイスティックの信号を受け取ったら呼ばれる関数 */
void KobukiOperation::joyCallback(const sensor_msgs::Joy::ConstPtr &joy)
{
    //変数の宣言(int a; とかと一緒)
    //topicの型に合わせた変数を定義
    geometry_msgs::Twist twist;

    // joyの信号を使いたいときはこんな感じ
    twist.linear.x  = scaleLinear * (joy->buttons[0] -joy->buttons[2]);
    twist.angular.z = scaleAngular *(joy->buttons[1] -joy->buttons[3]);

    // 発展課題
    // if(attack==1){
    //     ROS_INFO_STREAM("Attack");
    //     twist.linear.x  = 0.0;
    //     twist.angular.z = 0.0;
    //     }

    kobuki_pub.publish(twist);
}

/* main関数(そのままでよい) */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "kobuki_operation");
    KobukiOperation kobuki_operation;
    ros::spin();
}
