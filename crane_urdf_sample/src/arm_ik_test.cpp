﻿// 運動学計算があっているか確認するためのコード

#include <ros/ros.h>
#include "arm_ik.h"

///リンク長の設定
int main(int argc, char **argv) {
  float link1 = 0.0454;
  float link2 = 0.026;
  float link3 = 0.083;
  float link4 = 0.0935;
  float link5 = 0.1;
  ArmMock arm_mock(link1, link2, link3, link4, link5);
  ArmSolver arm_solver(link1, link2, link3, link4, link5);

///目標手先位置の設定
  geometry_msgs::Point target_point;
  target_point.x = 0.2;
  target_point.y = 0.15;
  target_point.z = 0.1;
  ROS_INFO("input pos: %f, %f, %f", target_point.x, target_point.y, target_point.z);///設定した目標手先位置を表示
    
  Angle4D angles;
  if(arm_solver.solve(target_point, 1.5707, angles)){ ///逆運動学計算
    arm_mock.setAngle(angles); ///逆運動学計算に基づく目標関節角度を設定
    ROS_INFO("target angles: %f, %f, %f, %f", angles.angle1, angles.angle2, angles.angle3, angles.angle4，angles.angle5);///逆運動学の解を表示
    geometry_msgs::Point output_point = arm_mock.getTargetPoint(); ///逆運動学で導出した解に基づき順運動学を計算
    ROS_INFO("Point by target angles: %f, %f, %f", output_point.x, output_point.y, output_point.z); ///逆運動学の解を用いた手先位置を表示
  }
  else{
    ROS_INFO("can not solve");
  }
   
  return 0;
}