﻿#pragma once

#include<ros/ros.h>
#include<geometry_msgs/Point.h>
#include<sensor_msgs/JointState.h>
#include<string>
#include<cmath>
#include<limits>

struct Angle4D{
    float angle1;
    float angle2;
    float angle3;
    float angle4;
    float angle5;
};

class ArmMock{ ///現在の関節角度情報取得と設定
public:
///リンク長情報の設定
    ArmMock(float l1, float l2, float l3, float l4, float l5){
        length1_ = l1;
        length2_ = l2;
        length3_ = l3;
        length4_ = l4;
        length5_ = l5;
    }

///
    void setAngle(float angle1, float angle2, float angle3, float angle4, float angle5){
        arm1_angle_ = angle1;
        arm2_angle_ = angle2;
        arm3_angle_ = angle3;
        arm4_angle_ = angle4;
        arm5_angle_ = angle5;
    }

    void setAngle(Angle4D angles){
        arm1_angle_ = angles.angle1;
        arm2_angle_ = angles.angle2;
        arm3_angle_ = angles.angle3;
        arm4_angle_ = angles.angle4;
        arm5_angle_ = angles.angle5;
    }

    sensor_msgs::JointState getJointState(){
        sensor_msgs::JointState output;
        output.header.stamp = ros::Time::now();
        output.name.resize(5);
        output.name[0] = arm1_joint_name_;
        output.name[1] = arm2_joint_name_;
        output.name[2] = arm3_joint_name_;
        output.name[3] = arm4_joint_name_;
        output.name[4] = arm5_joint_name_;
        output.position.resize(5);
        output.position[0] = arm1_angle_;
        output.position[1] = arm2_angle_;
        output.position[2] = arm3_angle_;
        output.position[3] = arm4_angle_;
        output.position[4] = arm5_angle_;
        return output;
    }

///順運動学計算(計算結果がoutput.x, output.y, output,zとして返ってくる)
    geometry_msgs::Point getTargetPoint(){
        geometry_msgs::Point output;
        float l = length3_ * sin(arm2_angle_) + length4_ * sin(arm2_angle_ + arm3_angle_) + length5_ * sin(arm2_angle_ + arm3_angle_ + arm4_angle_);
        float z = length1_ + length2_ + length3_ * cos(arm2_angle_) + length4_ * cos(arm2_angle_ + arm3_angle_) + length5_ * cos(arm2_angle_ + arm3_angle_ + arm4_angle_);
        output.x = l * cos(arm1_angle_);
        output.y = l * sin(arm1_angle_);
        output.z = z;
        return output;
    }

///リンク、ジョイント情報の初期化
    float length1_ = 0.1;
    float length2_ = 0.1;
    float length3_ = 0.1;
    float length4_ = 0.1;
    float length5_ = 0.1;

    float arm1_angle_ = 0.0;
    float arm2_angle_ = 0.0;
    float arm3_angle_ = 0.0;
    float arm4_angle_ = 0.0;
    float arm5_angle_ = 0.0;

    std::string arm1_joint_name_="joint1";
    std::string arm2_joint_name_="joint2";
    std::string arm3_joint_name_="joint3";
    std::string arm4_joint_name_="joint4";
    std::string arm5_joint_name_="joint5";
///
};

class ArmSolver{ ///逆運動学計算用のクラス
public:
    ArmSolver(float l1, float l2, float l3, float l4, float l5){
        length1_ = l1;
        length2_ = l2;
        length3_ = l3;
        length4_ = l4;
        length5_ = l5;
    }

///逆運動学計算用の関数
    bool solve(geometry_msgs::Point point_msg, float target_angle, Angle4D& output){
        output.angle1 = ;/////各自計算/////
        output.angle2 = ;/////各自計算/////
        output.angle3 = ;/////各自計算/////
        output.angle4 = ;/////各自計算/////
        output.angle5 = 0;

        ///有効な解であればtrue
        if(checkAngleValid(output))return true;
        else return false;
    }

///答えが無効な値になっていないかのチェック用関数
    bool checkAngleValid(Angle4D angles){
        if(std::isnan(angles.angle1))return false;
        if(std::isnan(angles.angle2))return false;
        if(std::isnan(angles.angle3))return false;
        if(std::isnan(angles.angle4))return false;
        if(std::isnan(angles.angle5))return false;
        return true;
    }

    float length1_ = 0.1;
    float length2_ = 0.1;
    float length3_ = 0.1;
    float length4_ = 0.1;
    float length5_ = 0.1;
};

class ArmSmooth{ ///経由点計算用のクラス
public:
    ArmSmooth(void){}

    void setTargetAngles(Angle4D angles){
        start_angles_ = current_angles_;
        end_angles_ = angles;
    }

    void setCurrentAngles(Angle4D angles){
        current_angles_ = angles;
    }

    Angle4D output(float value){ ///経由点計算用の関数
        
        Angle4D output;

        ///経由する関節角度の設定
        output.angle1 = ;/////各自計算/////
        output.angle2 = ;/////各自計算/////
        output.angle3 = ;/////各自計算/////
        output.angle4 = ;/////各自計算/////
        output.angle5 = ;/////各自計算/////
        
        ///現在角度の更新
        current_angles_ = output;
        return output;
    }

    Angle4D start_angles_;
    Angle4D end_angles_;
    Angle4D current_angles_;    
};