﻿#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include "arm_ik.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "crane_ik_node");
///publishの設定
  ros::NodeHandle nh;
  ros::Publisher joint_state_pub = nh.advertise<sensor_msgs::JointState>("joint_states", 1);///関節角移動用のパブリッシュ
  ros::Publisher point_pub = nh.advertise<geometry_msgs::PointStamped>("target_point", 1);///目標手先位置確認用のパブリッシュ

///リンク長の設定  
  float link1 = 0.0454;
  float link2 = 0.026;
  float link3 = 0.083;
  float link4 = 0.0935;
  float link5 = 0.1;
  ArmMock arm_mock(link1, link2, link3, link4, link5);
  ArmSolver arm_solver(link1, link2, link3, link4, link5);
  ArmSmooth arm_smooth;

///関節角度初期化
  Angle4D init_angles;
  init_angles.angle1 = 0.0;
  init_angles.angle2 = 0.0;
  init_angles.angle3 = 0.0;
  init_angles.angle4 = 0.0;
  init_angles.angle5 = 0.0;
  arm_smooth.setCurrentAngles(init_angles);

  ros::Rate loop_rate(10);
  while(ros::ok()){
    geometry_msgs::PointStamped target_point;
    target_point.header.stamp = ros::Time::now();
    target_point.header.frame_id = "base_link";
   
///目標位置の設定
    target_point.point.x = 0.2;
    target_point.point.y = 0.15;
    target_point.point.z = 0.1;
    point_pub.publish(target_point);
    Angle4D angles;

    ///逆運動学計算 
    if(!arm_solver.solve(target_point.point, 1.5707, angles)){
      ROS_INFO("can not solve");
      break;
    }

    ///目標経由角度の計算とパブリッシュ
    arm_smooth.setTargetAngles(angles);
      arm_mock.setAngle(arm_smooth.output());  ///経由角計算用の関数(arm_smooth.output)と目標経由角設定用の関数(arm_mock.setAngle)
      joint_state_pub.publish(arm_mock.getJointState()); ///経由点の関節角度へパブリｯシュ
      ros::spinOnce();
      loop_rate.sleep();
    
  }
  return 0;
}